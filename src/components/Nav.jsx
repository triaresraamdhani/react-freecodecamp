import React from 'react'
import { Link } from 'react-router-dom'
import './nav.css'
import reactLogo from '../assets/image.png'
export default function Nav() {
  return (
    
    <nav>
      <div id='title'>
      <img 
          src={reactLogo} 
          className='nav-logo'
          alt='nav-logo'    
      />
      <h1>Tri's Documentation</h1>
      </div>
      
      <ul className='nav-item'>
        <li>home</li>
        <li>About</li>
        <li><button>Project</button></li>
      </ul>
    </nav>
    

  )
}
