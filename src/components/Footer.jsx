import React from 'react'
import './footer.css'
export default function Footer() {
  return (
    <footer>
        <small>@2022 Tri Development. All rights reserved.</small>
    </footer>
  )
}
