import React from 'react'
import emailIcon from '../../assets/section1/icon.png'
import linkedinIcon from '../../assets/section1/Vector.png'
import './button.css'

export default function Button() {
  return (
    <div className='button--container'>
        <div className='button--email'>
            <img src={emailIcon} alt="email-icon" className='email--icon'/>
            <p>Email</p>
        </div>
        <div className='button--linkedin'>
            <img src={linkedinIcon} alt="linkedin-icon" className='linkedin--icon'/>
            <p>Linkedin</p>
        </div>
    </div>
  )
}
