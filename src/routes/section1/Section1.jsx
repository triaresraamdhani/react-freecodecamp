import React from 'react'
import profilePicture from '../../assets/section1/tri.jpg/'
import facebook from '../../assets/section1/Facebook.png'
import github from '../../assets/section1/github.png'
import instagram from '../../assets/section1/instagram.png'
import twitter from '../../assets/section1/Twitter.png'
import Button from './button'
import styles from './section1.module.css'

export default function Section1() {
  return (
    <div className={styles.wrapper}>
      <div className={styles.container}>
        <img src={profilePicture} alt="profile" className={styles.profilePicture} />
        <div className={styles.containerContent}>
          <h1 className={styles.name}>Tri Ramdhani</h1>
          <h4 className={styles.ocupation}>Front End Developer</h4>
          <h4 className={styles.website}>tri.website</h4>

          <Button/>

          <div className={styles.contentAbout}>
            <h3 className={styles.titleContent}>About</h3>
            <p className={`${styles.detailContent} ${styles.p}`}>I am a frontend developer with a particular interest in making things simple and automating daily tasks. I try to keep up with security and best practices, and am always looking for new things to learn.</p>
          </div>
          <div className={styles.contentInterest}>
            <h3 className={styles.titleCotentInterest}>Interest</h3>
            <p className={`${styles.detailContentInterest} ${styles.p}`}>Food expert. Music scholar. Reader. Internet fanatic. Bacon buff. Entrepreneur. Travel geek. Pop culture ninja. Coffee fanatic.</p>
          </div>
        </div>
        <div className={styles.footer}>
          <img src={twitter} alt="twitter-icon" className={styles.twitterIcon} />
          <img src={instagram} alt="instagram-icon" className={styles.instagramIcon} />
          <img src={facebook} alt="facebook-icon" className={styles.facebookIcon} />
          <img src={github} alt="github-icon" className={styles.githubIcon} />
        </div>
      </div>
    
    
    </div>
  )
}
