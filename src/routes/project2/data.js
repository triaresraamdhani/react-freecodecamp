export default [
    {   
        id : 1,
        img :"image.png",
        rating : '5.0',
        reviewCount : 6,
        location : "Online",
        title : "Life Lesson With Kattie Zafires",
        price : 136,
        openSpots : 0
    },
    {
        id : 2,
        img :"wdding.png",
        rating : '5.0',
        reviewCount : 30,
        location : "Online",
        title : "Learn wedding photography",
        price : 125,
        openSpots : 3
    },
    {   
        id : 3,
        img :"bike.png",
        rating : '4.8',
        reviewCount : 2,
        location : "Offline",
        title : "Group mountain bike",
        price : 50,
        openSpots : 3
    }
]