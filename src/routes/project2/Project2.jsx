import React from 'react'
import Cards from './Cards'
import Hero from './Hero'
import Navbar from './Navbar'
import data from'./data'
import './project2.css'

export default function Project2() {
  const cards = data.map((item) => {
    return ( 
    <Cards 
      key={item.id}
      {...item}
    />)
   }) 
  return (
      <>
        <Navbar/>
        <Hero/> 
        <section className='card--list'>
          {cards}
        </section>
        {/* <Cards
          img ="image.png"
          rating = '5.0'
          reviewCount = {6}
          country = "USA"
          title = "Life Lesson With Kattie Zafires"
          price = {136}
        /> */}
      </>
    )
}