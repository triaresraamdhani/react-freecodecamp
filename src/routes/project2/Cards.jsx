import React from 'react'
import './Cards.css'
// import ProfilePicture from './image.png'
import starIcon from '../../assets/project2/Star.png'

export default function Cards({
  openSpots,
  location,
  img,
  rating,
  reviewCount,
  title,
  price,
}) {
    let badgeText 
    if(openSpots === 0){
      badgeText = 'SOLD OUT'
    }else if(location === "Online") {
      badgeText = 'Online'
    }
  return (
    <div className='card'>
      {badgeText && <div className="card--badge">{badgeText}</div>}
        <img src= {`/src/assets/project2/${img}`} alt="card-image" className='card--image'/>
        <div className="card--wraper">
            <div className="card--stats">
                <img src={starIcon} className='card--star'/>
                <span>{rating}</span>
                <span className='gray'>({reviewCount})</span>
                <span className='gray'>{location}</span>
            </div>
            <p className='card--title'>{title}</p>
            <p><span className='bold'>from {price} </span>/ person</p>
        </div>
    </div>
  )
}
