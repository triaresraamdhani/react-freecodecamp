import React from 'react'
import './Hero.css'
import heroImage from '../../assets/project2/Group.png'

export default function Hero() {
  return (
    <div className='wraper'>
    <section className='hero'>
        <img src={heroImage} alt="hero-image" className='hero--image'/>
    </section>
        <h1 className='hero--header'>Online Experience</h1>
        <p className='hero--text'>Join Unique interactive activities lead by one-of-a-kinf hosts-all without leacving home.</p>
    </div>
  )
}
