import React, {useState} from 'react'
import Like from './like'

export default function State() {
  const [contact, setContact] = React.useState({
    firstName: "Jhon",
    lastName: "Doe",
    phone: "0998767578",
    emial: "dadjkafj@dlajfld",
    isFavorite: false
  })

  
  function toggle() {
    setContact((prevContact) => {
      return {
        ...prevContact,
        isFavorite
        : !prevContact.isFavorite
      }
    })
  }

  const [isGoingOut, setIsGoingOut] = useState(false)
  function flipValue() {
    setIsGoingOut((prevstate) => !prevstate)
  }

  return (
    <div>
      <p>{contact.firstName}{contact.lastName }</p>
      <p>{contact.phone }</p>
      <p>{contact.emial}</p>
      <Like isLiked={contact.isFavorite} handleClick={toggle} />
      

      <h1>do i feel like going out tonight?</h1>
      <div onClick={flipValue}>{ isGoingOut ? 'YES' : "NO"}</div>
    </div>
  )
}
