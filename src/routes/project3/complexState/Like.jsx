import React from 'react'

export default function Like(props) {
  let like = props.isLiked ? "liked": "like"
  return (
    
    <button onClick={props.handleClick} className={props.isLiked ? "btn-blue" : "btn-white"}>{like}</button>
  )
}
