import React, {useState } from 'react'
import State from './complexState/State'
import './meme.css'

export default function Meme() {
  const [meme, setMeme] = useState({
    topText: '',
    bottomtext: '',
    randomImage:''
  })
  const [allMemeImages, setAllMemeImages] = useState([])
  React.useEffect(()=> {
    async function fetchApi() {
      const res = await fetch('https://api.imgflip.com/get_memes')
      const data = await res.json()
      setAllMemeImages(data.data.memes)
    }
    fetchApi()
  }, [])
  function getImage() {
    const memesArray = allMemeImages
    const randomNumber = Math.floor(Math.random() * memesArray.length)

    const image = memesArray[randomNumber].url
    setMeme((prevState) => {
      return {
      ...prevState,
      randomImage : image
    }})
    
  }

  const [formData, setFormData] = useState(
    {topText: "" , bottomtext: ""}
  )
  function handleChange(event) {
    const {name , value} = event.target
    setFormData(prevState => {
      return {
        ...prevState,
        [name] : value
      }
    })

  }
  
  return (
    <main>
        
        <div className='form'>
            <input 
                type="text"
                placeholder='Top text'
                className='form--input'
                onChange={handleChange}
              name="topText"
            />
            <input 
                type="text"
                placeholder='Bottom text'
                className='form--input'
                onChange={handleChange}
                name="bottomText"
            />
            <button className='form--button' onClick={getImage}>Get a new meme image</button>
      </div>
      <div className="image-wraper">
        <img src={meme.randomImage} className='meme--image' />
        <span className='top--text'>{formData.topText }</span>
        <span className='bottom--text'>{formData.bottomText }</span>
      </div>
    </main>
  )
}
