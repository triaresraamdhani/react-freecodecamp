import React from 'react'
import Header from './header'
import Meme from './Meme'
import './project3.css'

export default function Project3() {
  return (
    <>
      <Header />
      <Meme />
    </>
  )
}
 