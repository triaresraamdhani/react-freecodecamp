import React from 'react'
import './header.css'

export default function Header() {
  return (
    <header className='header'>
        <img src="/src/assets/project3/Troll.png" alt=""  className='heder--image'/>
        <h2 className='heder--title'>Meme Generator</h2>
        <h4 className='heder--project'>React Course - Project 3</h4>
    </header>
  )
}
