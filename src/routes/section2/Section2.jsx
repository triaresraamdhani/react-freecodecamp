import React from 'react'
import './section2.css'
import data from './data'
import Widget from './Widget'

export default function Section2() {
  let widgets = data.map((item) => {
    return (
      <Widget
      key = {item.id}
      {...item}
      />
    )
  })
  return (
    <>
      <header>
        <img src="/src/assets/section2/Fillweb.png" className='web--icon'/>
        <span className='web--domain'>tri.web.co.id</span>
      </header>
      <main>
        {widgets}  
      </main>
    </>
  )
}
