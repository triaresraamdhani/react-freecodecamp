export default [
    {
        id : 1,
        title : "Mount Fuji",
        location : "JAPAN",
        imageUrl : "fuji.png",
        googleMap : "https://www.google.com/maps/d/u/0/viewer?mid=1W9AtcwnYAqP1yVHr5YjhwmQa0eM&hl=en&ll=35.36479441415818%2C138.763048&z=13",
        startDate : "12 Jan 2021",
        endDate : "24 Jan 2021",
        description : "Mount Fuji is the tallest mountain in Japan, standing at 3,776 meters (12,380 feet). Mount Fuji is the single most popular tourist site in Japan, for both Japanese and foreign tourists."
    },
    {
        id : 2,
        title : "Sydney Opera House",
        location : "AUSTRALIA",
        imageUrl : "australia.png",
        googleMap : "https://goo.gl/maps/j7heWaEQ5dTrkqxs7",
        startDate : "27 May 2021",
        endDate : "8 Jun 2021",
        description : "The Sydney Opera House is a multi-venue performing arts centre in Sydney. Located on the banks of the Sydney Harbour, it is often regarded as one of the 20th century's most famous and distinctive buildings"
    },
    {
        id : 3,
        title : "Geirangerfjord",
        location : "NORWAY",
        imageUrl : "norway.png",
        googleMap : "https://www.google.com/maps/d/u/0/viewer?hl=en_US&mid=1zi9bW9Zkl8ExIwx-ZNbeChUSK38&ll=62.091377%2C7.061932999999994&z=17",
        startDate : "01 Oct 2021",
        endDate : "18 Nov 2021",
        description : "The Geiranger Fjord is a fjord in the Sunnmøre region of Møre og Romsdal county, Norway. It is located entirely in the Stranda Municipality."
    }
]