import React from 'react'
import './widget.css'

export default function Widget(props) {
  return (
    <div className='wraper'>
        <img src={`/src/assets/section2/${props.imageUrl}`} className='title--image' />
        <div className="wraper--content">
            <div className="content--location">
                <img src="/src/assets/section2/Filllocation.png" className='location--icon' />
                <span className='country--location'>{props.location}</span>
                <a href={props.googleMap}>View on Google Map</a>
            </div>
            <h1 className='title'>{props.title}</h1>
            <p className='event--date'>
                <span>{props.startDate}</span> - 
                <span>{props.endDate}</span>
            </p>
            <p className='event--description'>{props.description}</p>
        </div>
    </div>
  )
}
