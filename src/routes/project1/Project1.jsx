import React from 'react'
import reactLogo from '../../assets/image.png'
import './project1.css'

export default function Project1() {
  const [ isDarkMode, setIsDarkMode ] = React.useState(true)
  
  const [style, setStyle] = React.useState([
    {
      backgroundColor: '#282D35',
      color: 'white'
    },
    {
      backgroundColor: '#21222A',
      color: 'white'
    }
  ])
  const lightMode = {
    backgroundColor: '#DEEBF8',
    color: 'black'
  }
  const navDarkMode = {
    backgroundColor: '#282D35',
    color: "white"
  }
  const bodyDarkMode = {
    backgroundColor: '#21222A',
    color: "white"
  }
  
  React.useEffect(() => {
  }, [isDarkMode])
  
  function darkMode() {
    setIsDarkMode(prevState=>!prevState)
  }

  return (
    <div className='wraper' style={isDarkMode ? bodyDarkMode : lightMode}>
      <nav className='project1_' style={isDarkMode ? navDarkMode : lightMode}>
            <img 
              src={reactLogo} 
              className='project1_nav--logo'
              alt='nav-logo'    
            />
            <h3 className='project1_nav--logo_text' style={isDarkMode? {} : lightMode }>ReactFacts</h3>
            <h4 className='project1_nav--title' style={isDarkMode? {} : lightMode }>React Course-Project 1</h4>
              <label className="switch">
          <input type="checkbox" onClick={darkMode} />
                <span></span>
              </label>
        </nav>
        <main style={isDarkMode? {}: {borderTop: '1px solid black'}}>
          <h1 className='project1_main--title' style={isDarkMode? {} : lightMode }>Fun fact About React</h1>
          <ul className='project1_main--facts' style={isDarkMode? {} : lightMode }>
            <li><a>Was first released in 2003</a></li>
            <li><a>Was originally created by Jordan Walke</a></li>
            <li><a>Has well over 100K stars on Github</a></li>
            <li><a>Is maintained by Facebook</a></li>
            <li><a>Powers thousends of enterprise apps, including mobile apps</a></li>
          </ul>
        </main>
    </div>
  )
}
