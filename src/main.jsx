import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import React from 'react'
import ReactDOM from 'react-dom/client'
import Home from './pages/Home'
import * as Routes from './routes'
import './index.css'

const router = createBrowserRouter([
  { path: '/', element: <Home /> },
  // { path: 'project1', element: <Routes.Project1 /> },
  // { path: 'project2', element: <Routes.Project2 /> },
  { path: 'project3', element: <Routes.Project3 /> },
  // { path: 'section1', element: <Routes.Section1 /> },
  // { path: 'section2', element: <Routes.Section2 /> }
])

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
)
