import { Link } from 'react-router-dom'

const Home = () => (
  <div>
    <ul>
      <li><Link to='/project1'>Project 1</Link></li>
      <li><Link to='/project2'>Project 2</Link></li>
      <li><Link to='/project3'>Project 3</Link></li>
      <li><Link to='/section1'>Section 1</Link></li>
      <li><Link to='/section2'>Section 2</Link></li>
    </ul>
  </div>
)

export default Home